package com.example.module3

import android.media.AudioAttributes
import android.media.MediaPlayer
import android.media.MediaRecorder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.Environment.DIRECTORY_DOCUMENTS
import android.view.View
import androidx.core.net.toUri
import kotlinx.android.synthetic.main.activity_audio.*
import java.io.File

class AudioActivity : AppCompatActivity() {

    var mediaRecorder: MediaRecorder? = null
    var mediaPlayer: MediaPlayer? = null
    var file: File? = null

    lateinit var startClick: View.OnClickListener
    lateinit var stopClick: View.OnClickListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio)

         startClick = View.OnClickListener {
            if (mediaPlayer != null)
                stopMediaPlayer()
            val file = File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOCUMENTS).path + "/WSR", "test.mp3")
            replaceFileMediaRecorder(file)
            shoot_audio.setOnClickListener(stopClick)
        }
        stopClick = View.OnClickListener {
            stopMediaRecorder()
            initMediaPlayer {
                it.start()
            }
            shoot_audio.setOnClickListener(startClick)
        }
        shoot_audio.setOnClickListener(startClick)
    }

    fun initMediaPlayer(onPreparedListener: MediaPlayer.OnPreparedListener){
        mediaPlayer = MediaPlayer.create(this, file!!.toUri())
        mediaPlayer!!.setOnPreparedListener {
            mediaPlayer!!.apply {
                setAudioAttributes(AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build())
            }
            onPreparedListener.onPrepared(it)
        }
    }

    fun initMediaRecorder(){
        mediaRecorder = MediaRecorder()
        mediaRecorder!!.apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
        }
    }

    fun replaceFileMediaRecorder(file: File){
        if (mediaRecorder != null)
            stopMediaRecorder()
        if (file.parentFile!!.exists())
            file.parentFile!!.mkdirs()
        initMediaRecorder()
        mediaRecorder!!.apply {
            setOutputFile(file)
            prepare()
            start()
        }
        this.file = file
    }

    private fun stopMediaPlayer(){
        if (mediaPlayer != null){
            mediaPlayer!!.apply {
                stop()
                reset()
                release()
            }
            mediaPlayer = null
        }
    }

    private fun stopMediaRecorder(){
        mediaRecorder?.apply {
            stop()
            reset()
            release()
        }
        mediaRecorder = null
    }
}