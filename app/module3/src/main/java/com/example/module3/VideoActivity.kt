package com.example.module3

import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.MediaController
import kotlinx.android.synthetic.main.activity_video.*
import okhttp3.MediaType

class VideoActivity : AppCompatActivity() {

    lateinit var videoUri: Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        shoot_video.setOnClickListener {
            videoUri = contentResolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, ContentValues())!!
            val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri)
            startActivityForResult(intent, 110)
        }
        shoot_video.setOnLongClickListener {
            startActivity(Intent(this, AudioActivity::class.java))
            return@setOnLongClickListener true
        }
        video.setMediaController(MediaController(this))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 110)
            if (resultCode == Activity.RESULT_OK) {
                video.setVideoPath(videoUri.toString())
                video.seekTo(1)
            }
    }
}