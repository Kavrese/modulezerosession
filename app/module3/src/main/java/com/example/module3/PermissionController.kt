package com.example.module3

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.core.app.ActivityCompat

class PermissionController(private val activity: Activity) {
    val needPermissions = arrayOf(
        Manifest.permission.RECORD_AUDIO,
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    init {
        initNeedPermissions()
    }

    fun initNeedPermissions(){
        val permissionsNotGranted = getPermissionsNotGranted()
        requestPermissions(permissionsNotGranted, 100)
    }

    fun getPermissionsNotGranted(): Array<String>{
        return checkPermissions(needPermissions).filter{!it.value}.keys.toTypedArray()
    }

    fun checkPermissions(permissions: Array<String>): Map<String, Boolean>{
        return permissions.associate {
            Pair(
                it,
                ActivityCompat.checkSelfPermission(
                    activity,
                    it
                ) == PackageManager.PERMISSION_GRANTED
            )
        }
    }

    fun requestPermissions(permissions: Array<String>, requestCode: Int){
        if (permissions.isNotEmpty())
            ActivityCompat.requestPermissions(activity, permissions, requestCode)
    }

    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == 100)
            if (getPermissionsNotGranted().isNotEmpty())
                Toast.makeText(activity, "Не все разрешения предоставленны", Toast.LENGTH_LONG).show()
    }
}