package com.example.module3

import android.os.Environment
import android.os.Environment.DIRECTORY_DOCUMENTS
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream

interface FileAPI{
    @Streaming
    @GET("")
    fun getFile(@Query("title") title: String): Call<ResponseBody>

    @Multipart
    @POST("")
    fun sendFile(@Query("title") title: String, @PartMap map: HashMap<String, RequestBody>): Call<ResponseBody>
}

object ConnectControllerAcceptPoint{
    val connectController = ConnectController()
}

class ConnectController {
    private val retrofitFile: FileAPI = initRetrofit("").create(FileAPI::class.java)

    interface OnGetData<T>{
        fun onGet(data: T)
        fun onFail(message: String)
    }

    companion object {
        fun initRetrofit(baseUrl: String): Retrofit{
            return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }

    fun requestGetFile(title: String, onGetData: OnGetData<ResponseBody>){
        retrofitFile.getFile(title).enqueue(object: Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful && response.body() != null)
                    onGetData.onGet(response.body()!!)
                else
                    onGetData.onFail("Body null")
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                onGetData.onFail(t.message!!)
            }
        })
    }

    fun uploadFileToServer(file: File, format: String, type: String, onGetData: OnGetData<Boolean>){
        val body = RequestBody.create(MediaType.parse("$type/$format"), file)
        val map = hashMapOf("file" to body)
        retrofitFile.sendFile(file.name, map).enqueue(object: Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                onGetData.onGet(response.isSuccessful)
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                onGetData.onFail(t.message!!)
            }
        })
    }

    fun saveFileToDisk(title: String, body: ResponseBody, onGetData: OnGetData<File>){
        Thread {
            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null
            try {
                val file = File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOCUMENTS).path + "/WSR/$title")
                inputStream = body.byteStream()
                outputStream = FileOutputStream(file)
                val reader = ByteArray(4096)
                while(true){
                    val read = inputStream.read()
                    if (read == -1)
                        break
                    outputStream.write(reader, 0, read)
                }
                outputStream.flush()
                onGetData.onGet(file)
            }catch (e: Exception){
                onGetData.onFail(e.message!!)
            }finally {
                inputStream?.close()
                outputStream?.close()
            }
        }
    }

}