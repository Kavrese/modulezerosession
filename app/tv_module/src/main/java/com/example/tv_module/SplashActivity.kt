package com.example.tv_module

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class SplashActivity : AppCompatActivity() {
    lateinit var sh: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        sh = getSharedPreferences("0", 0)
        Handler().postDelayed({
          if (getFirst()){
              setFirst()
              startActivity(Intent(this, RegActivity::class.java))
              finish()
          }else{
              startActivity(Intent(this, AuthActivity::class.java))
              finish()
          }
        }, 1500)
    }

    fun getFirst(): Boolean{
        return sh.getBoolean("isFirst", true)
    }

    fun setFirst(){
        return sh.edit().putBoolean("isFirst", false).apply()
    }
}