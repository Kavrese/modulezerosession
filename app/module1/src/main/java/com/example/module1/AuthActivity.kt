package com.example.module1

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        to_reg.setOnClickListener {
            startActivity(Intent(this, RegActivity::class.java))
        }

        auth.setOnClickListener {
            val login = login_auth.text.toString()
            val password = password_auth.text.toString()
            if (login.isEmpty()){
                createAlertDialogError(this, "Заполните поле логина").show()
                return@setOnClickListener
            }
            if (password.isEmpty()){
                createAlertDialogError(this, "Заполните поле пароля").show()
                return@setOnClickListener
            }
            ConnectControllerAcceptPoint.connectController.requestTestAuth(login, password, object: ConnectController.OnGetData<String>{
                override fun onGet(data: String) {
                    Info.token = data
                    startActivity(Intent(this@AuthActivity, MainActivity::class.java))
                    finish()
                }

                override fun onFail(message: String) {
                    createAlertDialogError(this@AuthActivity, message).show()
                }
            })
        }
    }

    private fun validateEmail(email: String): Boolean{
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}

fun createAlertDialog(context: Context, title: String, message: String): AlertDialog{
    return AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton("OK", null)
        .create()
}

fun createAlertDialogError(context: Context, message: String): AlertDialog{
    return createAlertDialog(context, "Ошибка", message)
}

fun createAlertDialogError(context: Context): AlertDialog{
    return createAlertDialog(context, "Ошибка", "Повторите попытку позже")
}