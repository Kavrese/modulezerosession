package com.example.module2

import org.osmdroid.util.GeoPoint
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

data class ModelNavigation(
    val routes: List<ModelGeometry>
)

data class ModelGeometry(
    val geometry: String,
    val distance: Double
)

interface NavigationAPI{
    @GET
    fun navigation(@Url url: String,
                   @Query("generate_waypoints") generate_waypoints: Boolean = false,
                    @Query("hide_hints") hide_hints: Boolean = true
    ): Call<ModelNavigation>
}


object ConnectControllerAcceptPoint{
    val connectController = ConnectController()
}

class ConnectController {
    private val retrofitNavigation: NavigationAPI = initRetrofit("http://route.madskills.ru/v1/route/").create(NavigationAPI::class.java)

    interface OnGetData<T>{
        fun onGet(data: T)
        fun onFail(message: String)
    }

    companion object {
        fun initRetrofit(baseUrl: String): Retrofit {
            return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

        fun parsePointsToURl(points: List<GeoPoint>): String {
            return points.joinToString(separator = ";"){ "${it.longitude},${it.latitude}" }
        }
    }

    fun requestNavigation(points: List<GeoPoint>, onGetData: OnGetData<ModelNavigation>){
        retrofitNavigation.navigation(parsePointsToURl(points)).enqueue(object: Callback<ModelNavigation>{
            override fun onResponse(
                call: Call<ModelNavigation>,
                response: Response<ModelNavigation>
            ) {
                if (response.isSuccessful && response.body() != null){
                    onGetData.onGet(response.body()!!)
                }else{
                    onGetData.onFail("Body null")
                }
            }

            override fun onFailure(call: Call<ModelNavigation>, t: Throwable) {
                onGetData.onFail(t.message!!)
            }

        })
    }
}