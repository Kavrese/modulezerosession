package com.example.module2

import android.content.Intent
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_map.*
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker

class MapActivity : AppCompatActivity() {

    lateinit var mapUtils: MapUtils
    var userMarker: Marker? = null
    lateinit var permissionController: PermissionController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        permissionController = PermissionController(this)
        mapUtils = MapUtils(this, map)
    }

    override fun onResume() {
        super.onResume()
        mapUtils.addLocationListener(object: MapUtils.OnLocationListener{
            override fun onFailGEO() {
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }

            override fun onLocationChanged(p0: Location) {
                if (userMarker != null)
                    userMarker!!.remove(map)
                userMarker = mapUtils.createMarker(GeoPoint(p0), ContextCompat.getDrawable(this@MapActivity, R.drawable.ic_baseline_person_pin_circle_24)
                ) { marker, mapView ->
                    true }
                map.overlays.add(userMarker)
            }
        })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionController.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}