package com.example.module2

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.location.LocationListener
import android.location.LocationManager
import org.osmdroid.bonuspack.utils.PolylineEncoder
import org.osmdroid.config.Configuration
import org.osmdroid.library.BuildConfig
import org.osmdroid.tileprovider.tilesource.XYTileSource
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.Polyline

class MapUtils(private val context: Context, private val map: MapView) {

    private val onLocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    private var onLocationListener: OnLocationListener? = null

    interface OnLocationListener: LocationListener{
        fun onFailGEO()
    }

    init {
        initSettingMap()
    }

    fun initSettingMap(){
        Configuration.getInstance().userAgentValue = BuildConfig.APPLICATION_ID
        map.setScrollableAreaLimitLatitude(MapView.getTileSystem().maxLatitude, MapView.getTileSystem().minLatitude, 0)
        map.setTileSource(createSource())
        map.minZoomLevel = 4.0
        map.maxZoomLevel = 20.0
        map.setBuiltInZoomControls(false)
        map.setMultiTouchControls(true)
        map.isVerticalMapRepetitionEnabled = false
        map.controller.setZoom(4.0)
    }

    fun createSource(): XYTileSource{
        return XYTileSource("MAD", 4, 20, 256, ".png", arrayOf(
            "https://geo.madskills.ru/osm/"
        ))
    }

    companion object {
        fun parseGeometry(geometry: String): List<GeoPoint>{
            return PolylineEncoder.decode(geometry, 10, false)
        }
    }

    fun createMarker(position: GeoPoint, icon: Drawable?, onMarkerClickListener: Marker.OnMarkerClickListener): Marker {
        val marker = Marker(map)
        marker.position = position
        marker.icon = icon
        marker.setOnMarkerClickListener(onMarkerClickListener)
        return marker
    }

    fun createPolyline(points: List<GeoPoint>, color: Int = Color.BLUE): Polyline{
        val polyline = Polyline()
        polyline.setPoints(points)
        polyline.color = color
        return polyline
    }

    @SuppressLint("MissingPermission")
    fun addLocationListener(onLocationListener: OnLocationListener){
        if (this.onLocationListener == null) {
            if (onLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                this.onLocationListener = onLocationListener
                onLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    1000, 10f, onLocationListener
                )
            } else onLocationListener.onFailGEO()
        }
    }

    fun removeLocationListener(){
        if (this.onLocationListener != null) {
            onLocationManager.removeUpdates(onLocationListener!!)
            onLocationListener = null
        }
    }

    fun changeIcon(marker: Marker, icon: Drawable?){
        marker.icon = icon
        map.invalidate()
    }
}