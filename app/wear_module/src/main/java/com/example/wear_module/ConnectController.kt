package com.example.wear_module

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.POST
import retrofit2.http.Query

data class ModelTestAuth(
    val notice: Notice
)

data class Notice(
    val error: String?,
    val token: String?,
    val answer: String?
)
interface TestAuthAPI{
    @POST("login")
    fun login(@Query("username") username: String, @Query("password") password: String): Call<ModelTestAuth>
}
object ConnectControllerAcceptPoint{
    val connectController = ConnectController()
}

class ConnectController {
    private val retrofitTestAuth: TestAuthAPI = initRetrofit("http://cars.areas.su/").create(TestAuthAPI::class.java)

    interface OnGetData<T>{
        fun onGet(data: T)
        fun onFail(message: String)
    }

    companion object {
        fun initRetrofit(baseUrl: String): Retrofit{
            return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }

    fun requestTestAuth(username: String, password: String, onGetData: OnGetData<String>){
        retrofitTestAuth.login(username, password).enqueue(object: Callback<ModelTestAuth>{
            override fun onResponse(call: Call<ModelTestAuth>, response: Response<ModelTestAuth>) {
                if (response.isSuccessful && response.body() != null){
                    val notice = response.body()!!.notice
                    when {
                        notice.token != null -> onGetData.onGet(notice.token)
                        notice.error != null -> onGetData.onFail(notice.error)
                        notice.answer != null -> onGetData.onFail(notice.answer)
                        else -> onGetData.onFail("Unknown error")
                    }
                }else{
                    onGetData.onFail("Body null")
                }
            }

            override fun onFailure(call: Call<ModelTestAuth>, t: Throwable) {
                onGetData.onFail(t.message!!)
            }

        })
    }
}